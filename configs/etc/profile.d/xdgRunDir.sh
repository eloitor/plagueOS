#!/bin/bash
#check to see if environment variable already set. 
if [ -z "$XDG_RUNTIME_DIR" ]; then  # It's not already set
  XDG_RUNTIME_DIR=/run/user/$UID  # Try default created path
  if [ ! -d "$XDG_RUNTIME_DIR" ]; then
    # default directory doesn't exist
    XDG_RUNTIME_DIR=/tmp/$UID-runtime
    if [ ! -d "$XDG_RUNTIME_DIR" ]; then  # Doesn't already exist
      mkdir -m 0700 "$XDG_RUNTIME_DIR"
    fi
  fi
  export XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR
fi

umask 0077
ulimit -c 0