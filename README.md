# plagueOS

Void musl build with hardening configurations aimed at rendering adversarial attacks ineffective by mitigating classes of exploitation that plague standard GNU/Linux systems.

*"But what does it mean? The plague? It's life, that's all." - A. Camus*


# Install Guide

## Prerequisites
1. Internet Connection
2. UEFI
3. Recommended minimum 8GB of RAM, 50GB of storage

## Flash USB
- Download Void musl live image from https://voidlinux.org/download/
	- Validate with PGP key or the SHA256 sum listed with the ISO files
	- Flash the .iso file to a USB
		- For Windows, download Balena Etcher to flash
		- For Linux, run the following dd command:
			- Run  `sudo fdisk -l` to show the flashdrive
			- Run `sudo dd if=void*.iso of=/dev/<USB>`
				- example: `/dev/sdb`
			- After flash is complete, run `umount /dev/<USB>` to unmount the drive

## Base Install & Hardening
 - Plug in the flashdrive and boot to the USB (typically ESC/F9-F12 allow you to select boot device upon system startup)
 - Start a live session in void
    - Sign in with credentials: `root/voidlinux`
    - `xbps-install -S xbps git`
    - `git clone -b master --single-branch https://git.envs.net/whichdoc/plagueOS.git`
    - `bash plagueos/plague-install`
    - The ISO will be released shortly where the command `plague-install` can be executed.

### Install Options
 - During the install, multiple configurations will be displayed. The selections are between the following:
	- `Cli-only` (Minimal installation for advanced users)
	- `Sway WM` (Minimalist Windows Manager (WM) for advanced users)
	- `Gnome w/ Wayland` (For novice users who desire gutted desktop environment)
		- Note: `Gnome w/ Wayland` has an active bug with `GDM` that will disrupt sessions upon lock/hibernate/suspend actions. To get back in your session following a lock, press `CTRL+ALT+F7.`
	- `Gnome w/ LightDM` (Heaviest but relatively minimalist option. This is a workaround while the `GDM` is being resolved. 
		- Note: You will need to set up your own keyboard shortcuts with LightDM. From `gnome-control-center`, you can add custom shortcuts such as screenlock by entering the command `dm-tool lock` into the shortcut prompt.
        
## Implementation Goals
- [X] Full System Build
- [X] Dependency Install
- [X] Hardened Memory Allocator to system-wide LD_PRELOAD
- [X] Hardened Kernel
- [X] Blacklisted Kernel Modules
- [X] Blacklisted File Systems
- [X] Blacklisted Network Protocols
- [X] IPTable Packet Filtering
- [X] Custom LUKS Encryption (AES256XTS+Argon2id KDF)
- [X] Locked Root Account
- [X] Hardened Boot Parameters
- [X] Hide Proc IDs
- [X] UMASK 0077 default system wide
- [X] admin account for elevated privileges
- [X] Secure fstab config (Bind for var and tmp)
- [X] Roll in Whonix hide-hardware-info (See [here](https://codeberg.org/SalamanderSecurity/PARSEC/src/branch/main/parsec/bin/hide-hardware-info))
- [X] bwrap profiling for building VM's
- [X] Full Wayland implementation (no Xorg)
- [X] VM startup script testing
- [X] VM Build script testing
- [X] Launch `plague-menu` script on boot. BashRC/sway config implementation
- [X] bwrap profiling
- [X] Basic PAM implementation
- [X] Allow User selection between WM, DE, or CLI-only
- [X] Use of doas over sudo
- [X] Generic Machine ID
- [X] Randomize MAC address for NIC
- [X] Memory erasure/poisoning
- [ ] Feature: Implement WiFi Interface softblock (psec-rfkill)
- [ ] Feature: bwrap of all executables used by user
- [ ] Feature: Nuke LUKS keys given secondary passphrase (See reference [here](https://gitlab.com/kalilinux/packages/cryptsetup-nuke-keys))
- [ ] Feature: Add VM artifacts to main host to trick malware (WIP)
- [X] All commits contain PGP signatures
- [ ] Minisign / PGP signatures for tar'd repository
- [ ] Audit of Install Script (Ongoing)

## Trust Model 
It is important to operate on a zero trust model. Since this is an impossible feat, it should at least be known where trust is given and impose restrictions/limitations where feasible. 
- Hardware
    - CPU
    - Miscellaneous Integrated Chipsets
- Software
    - Hypervisor ([Void OS](https://voidlinux.org/))
        - Void Developers
    - PlagueOS Script Build 
    - [SwayWM](https://swaywm.org) - Option during install
    - [GNOME Core](https://www.gnome.org/) - Option during install
		- Default GDM (Wayland only) - Option 1
		- LightDM (Wayland + Xorg) - Option 2
    - Graphene's [Hardened Malloc](https://github.com/GrapheneOS/hardened_malloc) 
    - Guest OS of Choice (varying levels of security/restrictions based on VMs intended user)
		- [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM)
        - [Whonix](https://www.whonix.org/wiki/KVM)
        - [ParrotOS](https://parrotsec.org/)
        - [Arch](https://archlinux.org/)
        - Any other OS desired by user. 

## Schema

    --> /dev/sda
        --> /dev/sda1   EFI Partition   /boot
        --> /dev/sda2   Linux LVM
            --> Crypt  Luks2 encrypted drive
                --> LogicalVolume_Home      Home Partition      /home
                --> LogicalVolume_Root      Root Partition      /
                --> LogicalVolume_Var       Var Partition       /var
                --> LogicalVolume_Swap      Swap Partition      [Swap]


## Usage
This OS is designed to run as a hypervisor and launch Guest virtual machines (VMs); The OS is designed to be a minimal build and have a restricted userspace. The guests are launched from `virt-manager` 
- Run Virt-Manager either by opening a terminal and running the command `virt-manager` or opening the application via GUI
- During the installation, the option to pull down and import [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM) which is the clearnet baseline of Whonix for day-to-day activities. 
- If the torrified version of Kicksecure - [Whonix](https://www.whonix.org/wiki/KVM) - is chosen, you must boot both the `Whonix-Gateway` and `Whonix-Workstation` with `virt-manager`
	- Note: If the `Whonix-Gateway` is down, the `Whonix-Workstation` will not have internet access. 


Feel free to contact us in the PlagueOS matrix [chat](https://matrix.to/#/!BAkNDIUQRnHPJwkwsq:matrix.org?via=matrix.org)

## Creating void .iso for plagueOS install
 - Requirement: [void-mklive](https://github.com/void-linux/void-mklive.git) repository
 - `./mklive.sh -a x86_64-musl -o plagueOS.iso -p "base-devel xz curl gnupg2 tor cryptsetup lvm2" -T "plagueOS" -v linux5.10 -I includedir -C "nomodeset"`
 	- Note: Need to create a directory 'includedir' in void-mklive repo and add in plagueOS files.
